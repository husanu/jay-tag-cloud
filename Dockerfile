FROM node:6-alpine

ENV NODE_ENV 'production'

# Create app directory
RUN mkdir -p /srv/app
WORKDIR /srv/app

# Copy sources
COPY . /srv/app

RUN npm install --silent supervisor -g
RUN npm install --silent

EXPOSE 3000

CMD [ "supervisor", "-V", "-w", "index.js", "index.js" ]
