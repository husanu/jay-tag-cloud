var lg = console.log;

var express = require('express');
var morgan = require('morgan');
var app = express();
  
// All default options
var poet = require('poet')(app, {
	postsPerPage: 2,
	posts: './posts',
	metaFormat: 'json',
});

poet.watch(function () {
	process.exit(0);
});

poet.init().then(function () {
  // initialized
  lg('poet is up');
});

poet.addRoute('/tags', function (req, res) {
  res.render('tags', {
  });
});

app.set('view engine', 'jade');
app.set('views', __dirname + '/views');

app.use(morgan('combined'));
app.use(express.static(__dirname + '/public'));
app.get('/', function (req, res) { res.render('index'); });

app.use(function (req, res) { 
	res.send('404');
});

app.listen(3000, function (err) {
	lg('listening')
});

setInterval(function () {
	process.stdout.write('.');
}, 1000*60);
